#include "DateTime.h"
#include <iostream>

bool G_DEBUG = false;

/// <summary>
/// ���������� ���� ��-��������� (2000.01.01 00:00)
/// </summary>
DateTime::DateTime()
{
	if (G_DEBUG) std::cout << "[DEBUG]: Default Constructor\n";

	dateTime_str = new std::string();

	year = 2000;
	month = 1;
	day = 1;

	hour = 0;
	minute = 0;
}

/// <summary>
/// ���������� �������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
DateTime::DateTime(int year, int month, int day, int hour, int minute)
{
	if (G_DEBUG) std::cout << "[DEBUG]: Constructor with params\n";

	dateTime_str = new std::string();

	this->setDate(year, month, day);
	this->setTime(hour, minute);
}

/// <summary>
/// ���������� ����� ����
/// </summary>
/// <param name="dateTime">����</param>
DateTime::DateTime(const DateTime& dateTime)
{
	if (G_DEBUG) std::cout << "[DEBUG]: Copy Constructor\n";

	dateTime_str = new std::string();

	this->setDate(dateTime.year, dateTime.month, dateTime.day);
	this->setTime(dateTime.hour, dateTime.minute);
}

DateTime::~DateTime()
{
	if (G_DEBUG) std::cout << "[DEBUG]: Destructor\n";

	delete dateTime_str;
}

/// <summary>
/// ���������� ���������� ���� � ������
/// </summary>
/// <param name="year">���</param>
/// <param name="month">���������� ����� ������</param>
/// <returns></returns>
int DateTime::getDaysInMonth(int year, int month)
{
	int daysCount = _days[month - 1];
	if (month == 2 && year % 4 == 0) daysCount++;

	return daysCount;
}

/// <summary>
/// ������������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
void DateTime::setDate(int year, int month, int day)
{
	validateDate(year, month, day);

	this->year = year;
	this->month = month;
	this->day = day;
}

/// <summary>
/// ������������� �����
/// </summary>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
void DateTime::setTime(int hour, int minute)
{
	validateTime(hour, minute);
		
	this->hour = hour;
	this->minute = minute;
}

/// <summary>
/// ���������� ��������� ������������� ����
/// </summary>
/// <returns></returns>
std::string DateTime::toStr()
{
	dateTime_str->clear();

	dateTime_str->append(std::to_string(year) + '.');
	if (month < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(month) + '.');
	if (day < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(day) + ' ');

	if (hour < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(hour) + ':');
	if (minute < 10) dateTime_str->append("0");
	dateTime_str->append(std::to_string(minute));

	return *dateTime_str;
}

/// <summary>
/// ������������ ���� � ������
/// </summary>
/// <param name="datetime">����</param>
/// <returns></returns>
std::string DateTime::dateTimeToStr(DateTime datetime)
{
	return datetime.toStr();
}

/// <summary>
/// ������� ���� �� �����
/// </summary>
void DateTime::print()
{
	std::cout << this->toStr() << '\n';
}

/// <summary>
/// ��������� � ���� ��������� ���������� ����. ����� ��������� ������������� ���������
/// </summary>
/// <param name="day">���������� ����</param>
/// <param name="hour">���������� �����</param>
/// <param name="minute">���������� �����</param>
void DateTime::addInterval(int day, int hour, int minute)
{
	validateTime(abs(hour), abs(minute));

	// ���������� ��������� � ������� ����
	this->minute += minute;
	this->hour += hour;
	this->day += day;
	
	// �������� ������������ �����
	// � ����������/���������� ����
	if (this->minute < 0) {
		this->minute += 60;
		this->hour--;
	} else if (this->minute > 59) {
		this->minute -= 60;
		this->hour++;
	}

	// �������� ������������ �����
	// � ����������/���������� ���
	if (this->hour < 0) {
		this->hour += 24;
		this->day--;
	} else if (this->hour > 23) {
		this->hour -= 24;
		this->day++;
	}

	// �������� ������������ ����
	// � ����������/���������� ������, ����
	while (this->day < 1) {
		if (this->month == 1) {
			this->year--;
			this->month = 12;
			this->day += 31;
		} else {
			this->month--;
			this->day += getDaysInMonth(this->year, this->month);
		}
	}
	while (this->day > getDaysInMonth(this->year, this->month)) {
		if (this->month == 12) {
			this->year++;
			this->month = 1;
			this->day -= 31;
		} else {
			this->day -= getDaysInMonth(this->year, this->month);
			this->month++;
		}
	}
}

/// <summary>
/// ��������� ���������� ����
/// </summary>
/// <param name="year">���</param>
/// <param name="month">�����</param>
/// <param name="day">����</param>
void DateTime::validateDate(int year, int month, int day)
{
	if (year < 0 || year > 2100) throw std::string("incorrect year");
	if (month < 0 || month > 12) throw std::string("incorrect month");
	if (day < 0 || day > getDaysInMonth(year, month)) throw std::string("incorrect day");
}

/// <summary>
/// ��������� ���������� �������
/// </summary>
/// <param name="hour">���</param>
/// <param name="minute">������</param>
void DateTime::validateTime(int hour, int minute)
{
	if (hour < 0 || hour > 23) throw std::string("incorrect hour");
	if (minute < 0 || minute > 59) throw std::string("incorrect minute");
}