#pragma once
#include <string>

extern bool G_DEBUG; // ����� ���������� ����������

class DateTime
{
public:
	DateTime();
	DateTime(int year, int month, int day, int hour, int minute);
	DateTime(const DateTime& dateTime);

	~DateTime();

	int getYear() { return year; }
	int getMonth() { return month; }
	int getDay() { return day; }
	int getHour() { return hour; }
	int getMinute() { return minute; }

	void setDate(int year, int month, int day);
	void setTime(int hour, int minute);

	void addInterval(int day = 0, int hour = 0, int minute = 0);

	std::string toStr();
	static std::string dateTimeToStr(DateTime datetime);

	void print();

private:
	int year;
	int month;
	int day;
	int hour;
	int minute;
	std::string* dateTime_str;

	int _days[12] = {31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}; // ������ ���������� ���� �� �������
	
	int getDaysInMonth(int year, int month);
	
	void validateDate(int year, int month, int day);
	void validateTime(int hour, int minute);
};