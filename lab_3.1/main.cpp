﻿#include <iostream>
#include "DateTime.h"

int main()
{
    try
    {
        DateTime d1 = DateTime();                       // Вызов конструктора по умолчанию
        DateTime d2 = DateTime(2022, 12, 1, 14, 00);    // Вызов конструктора с параметрами
        DateTime d3 = DateTime(d2);                     // Вызов конструктора копирования

        std::cout << "Date 1: "; d1.print();
        std::cout << "Date 2: "; d2.print();
        std::cout << "Date 3: "; d3.print();
        std::cout << "-----------------------------------------------\n";

        d3.setDate(2022, 12, 31);
        d3.setTime(23, 55);
        std::cout << "update Date 3: "; d3.print();
        std::cout << "-----------------------------------------------\n";

        std::cout << "Date 3";
        std::cout << "\nYear: " << d3.getYear();
        std::cout << "\nMonth: " << d3.getMonth();
        std::cout << "\nDay: " << d3.getDay();
        std::cout << "\nHour: " << d3.getHour();
        std::cout << "\nMinute: " << d3.getMinute();
        std::cout << "\n-----------------------------------------------\n";

        std::cout << "Date 3 + (800days 5min): ";
        d3.addInterval(800, 0, 5);
        d3.print();
        std::cout << "-----------------------------------------------\n";
        std::cout << "Date 3 - (800days 5min): ";
        d3.addInterval(-800, 0, -5);
        d3.print();
        std::cout << "-----------------------------------------------\n";

        std::cout
            << "new Date: "
            << DateTime::dateTimeToStr(
                DateTime(2023, 12, 2, 13, 30)   // Вызов конструктора с параметрами
            )                                   // Вызов деструктора анонимного объекта
            << '\n';
    }
    catch (const std::string e)
    {
        std::cout << "[ERROR]: " << e << '\n';

        return -1;
    }

    return 0;   // Вызов деструкторов объектов d1, d2, d3
}